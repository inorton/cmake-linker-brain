#include "a.h"
#include "b.h"

#include <stdio.h>

int a_function(int x) {
  return x * b_function();
}

void a_logger(const char* category, const char* msg) {
  static int lognum = 0;
  printf("%03d: %s\t %s\n", lognum++, category, msg);
}