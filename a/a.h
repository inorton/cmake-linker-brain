#ifndef _A_H
#define _A_H

int a_function(int x);

void a_logger(const char* category, const char* msg);

#endif
