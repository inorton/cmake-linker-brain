#include <stdio.h>
#include "b.h"
#include "a.h"


int main(int argc, char** argv) {

    a_logger("test", "starting test");
    a_logger("test", "b library:");
    a_logger("test", get_b_version());

    printf("a_function(1) returned %d\n", a_function(1));
    printf("b_function = %p, returned %d\n", b_function, b_function());

    printf("a_function(1) returned %d\n", a_function(1));
    printf("a_function(1) returned %d\n", a_function(1));
    printf("a_function(1) returned %d\n", a_function(1));

    a_logger("test", "end test");

    return 0;
}