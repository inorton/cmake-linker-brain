add_library(bbb STATIC bbb.c)
target_link_libraries(bbb PRIVATE bb)

file(WRITE ${CMAKE_BINARY_DIR}/empty.c "")


add_library(bbb-shared SHARED ${CMAKE_BINARY_DIR}/empty.c)
target_link_libraries(bbb-shared
        PUBLIC
            b-interface
        PRIVATE
            bbb)

if (WIN32)
    set_target_properties(bbb-shared PROPERTIES
            LINK_FLAGS "/WHOLEARCHIVE /NODEFAULTLIB"
            )
elseif (APPLE)
    set_target_properties(bbb-shared PROPERTIES
            LINK_FLAGS "-Wl,-all_load"
            )
else ()
    set_target_properties(bbb-shared PROPERTIES
            LINK_FLAGS "-Wl,--whole-archive -nostdlib -Wl,-Bsymbolic"
            )
endif()