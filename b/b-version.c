#include "b.h"

static const char* B_VERSION = "rel-1.0";

const char* get_b_version(void) {
  return B_VERSION;
}