#include "b.h"
#include "a.h"

int b_function(void) {
  static int i = 0;
  int result;
  a_logger(get_b_version(), "calling b_function " __FILE__);
  result = i++;
  a_logger(get_b_version(), "return  b_function " __FILE__);
  return result;
}
